// Fill out your copyright notice in the Description page of Project Settings.
/// The Punch - Part 2
#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "AttackStartNotifyState.generated.h"

/**
 * 
 */
UCLASS()
class CULTYGAME_API UAttackStartNotifyState : public UAnimNotifyState
{
	GENERATED_BODY()

public:
	virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;
	virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
/// The Punch - Part 2